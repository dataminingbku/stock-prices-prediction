from flask import Flask, request, jsonify
from Models.SVR_TENSOR import main as svr_tensor_main
from Models.SVM import main as svr_main
from Models.Linear.main import main as LinearMain
import plot
from Preprocessing.preprocessing import pre_processing, get_date
from sklearn.metrics import mean_squared_error
import numpy as np

app = Flask(__name__)


def check_fields(data):
    return data['open'] and data['high'] and data['low'] and data['volume']


@app.route("/svr_tensor/predict", methods=['GET'])
def svr_tensor_predict():
    data = request.get_json()
    if not check_fields(data):
        return jsonify({
            "error": 400,
            "data": "Missing fields"
        })
    else:
        _input = [[data['open'], data['high'], data['low'], data['volume']]]
        ret = svr_tensor_main.predict(_input)
        return jsonify({
            "error": 200,
            "data": str(ret[0])
        })


@app.route("/svr_tensor/train", methods=['GET'])
def svr_tensor_train():
    svr_tensor_main.remove_trained_model()
    train_loss, test_loss = svr_tensor_main.train()
    plot.plot_loss(train_loss=train_loss, test_loss=test_loss)
    return jsonify({
        "error": 200,
        "data": {
            "train_loss": str(train_loss[-1]),
            "test_loss": str(test_loss[-1])
        }
    })


@app.route("/svr/train", methods=['GET'])
def svr_train():
    train_loss, test_loss = svr_main.svr_train()
    return jsonify({
        "error": 200,
        "data": {
            "train_acc": str(train_loss),
            "test_acc": str(test_loss)
        }
    })


def evaluation(y_test_pred, y_test):
    return np.sqrt(mean_squared_error(y_test, y_test_pred))


if __name__ == '__main__':
    print("1. Run API")
    print("2. Plot svr tensorflow")
    print("3. Plot svr")
    print("4. Train svr tensorflow")
    print("5. Train Linear")
    print("6. Test Linear ")
    print("7. Predict Linear")
    print("8. Plot split data")
    print("9. Evaluate svr")
    print("10. Evaluate svr tensorflow")
    x = int(input("Choose one: "))
    if x == 1:
        app.run()
    elif x == 2:
        x_train, y_train, x_test, y_test = pre_processing()
        date = get_date()
        y_train_pred = svr_tensor_main.predict_normalized(x_train)
        y_test_pred = svr_tensor_main.predict_normalized(x_test)
        plot.plot(date=date, y_train=y_train, y_test=y_test,
                  y_train_pred=y_train_pred, y_test_pred=y_test_pred)
    elif x == 3:
        x_train, y_train, x_test, y_test = pre_processing()
        date = get_date()
        y_train_pred = svr_main.svr_predict_normalized(x_train)
        y_test_pred = svr_main.svr_predict_normalized(x_test)
        plot.plot(date=date, y_train=y_train, y_test=y_test,
                  y_train_pred=y_train_pred, y_test_pred=y_test_pred)
    elif x == 4:
        svr_tensor_main.remove_trained_model()
        train_loss, test_loss = svr_tensor_main.train()
        plot.plot_loss(train_loss=train_loss, test_loss=test_loss)

    elif x == 5:
        LinearMain("train")

    elif x == 6:
        LinearMain("test")

    elif x == 7:
        LinearMain("predict")
    elif x == 8:
        LinearMain("plot")
    elif x == 9:
        x_train, y_train, x_test, y_test = pre_processing()
        y_test_pred = svr_main.svr_predict_normalized_no_inverse(x_test)
        print(evaluation(y_test_pred=y_test_pred, y_test=y_test))
    elif x == 10:
        x_train, y_train, x_test, y_test = pre_processing()
        y_test_pred = svr_tensor_main.predict_normalized_no_inverse(x_test)
        print(evaluation(y_test_pred=y_test_pred, y_test=y_test))


"""
from Models.SVM.main import main as SVMain
from Models.Linear.main import main as LinearMain

def main():
    print("Options:")
    print("1. train SVM")
    print("2. predict SVM")
    print("3. train Linear")
    print("4. test Linear")
    print("5. predict Linear ")
    print("6. plot split data")
    _in = int(input("Enter number: "))
    if _in == 1:
        SVMain("train")
    elif _in == 2:
        SVMain("predict")
    elif _in == 3:
        LinearMain("train")
    elif _in == 4:
        LinearMain("test")
    elif _in == 5:
        LinearMain("predict")
    elif _in == 6:
        LinearMain("plot")

if __name__ == "__main__":
    main()
"""
