"""
    This module descripes the options for running Stock Prices Prediction using
    SVM
"""
import os
import numpy as np
from joblib import load
from Preprocessing.preprocessing import pre_processing
from .train import train

scaler_path = os.path.dirname(
    os.path.abspath(__file__)) + "/../../Preprocessing/"

model_path = os.path.dirname(os.path.abspath(
    __file__)) + "/../../trained-svm.joblib"


def svr_train():
    x_train, y_train, x_test, y_test = pre_processing()
    model = train(_x=x_train, _y=y_train, _x_test=x_test, _y_test=y_test)
    return model.score(x_train, y_train), model.score(x_test, y_test)


def svr_predict(_input):
    scaler_x = load(scaler_path + "scaler_x")
    scaler_y = load(scaler_path + "scaler_y")
    model = load(model_path)
    _input = np.array(_input)
    _input = scaler_x.transform(_input.astype(float))
    ret = [model.predict(_input)]
    return scaler_y.inverse_transform(ret).reshape(-1)


def svr_predict_normalized(_input):
    scaler_y = load(scaler_path + "scaler_y")
    model = load(model_path)
    _input = np.array(_input)
    ret = [model.predict(_input)]
    return scaler_y.inverse_transform(ret).reshape(-1)


def svr_predict_normalized_no_inverse(_input):
    scaler_y = load(scaler_path + "scaler_y")
    model = load(model_path)
    _input = np.array(_input)
    return model.predict(_input)
