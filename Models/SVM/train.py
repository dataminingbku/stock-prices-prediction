"""
    This module descripes how to train using SVM model
"""
import time
from joblib import dump
from sklearn import svm


def train(_x, _y, _x_test, _y_test):
    """ Function for training """
    start = time.time()
    # 19510000
    var_c = 100
    eps = 0.1
    model = svm.SVR(kernel='linear', gamma='scale', C=var_c, epsilon=eps)
    model.fit(_x, _y)
    dump(model, 'trained-svm.joblib')
    end = time.time()
    print("Training finished in " + str(end - start) + " seconds")
    return model
