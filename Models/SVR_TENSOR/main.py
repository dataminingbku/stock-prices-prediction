import os
import shutil
from joblib import load
import numpy as np
from . import svr_tensorflow as svr_tensor

train_model_path = os.path.dirname(
    os.path.abspath(__file__)) + "/../../trained-models"

scaler_path = os.path.dirname(
    os.path.abspath(__file__)) + "/../../Preprocessing/"


def predict(_input):
    scaler_x = load(scaler_path + "scaler_x")
    scaler_y = load(scaler_path + "scaler_y")
    _input = np.array(_input)
    _input = scaler_x.transform(_input.astype(float))
    ret = svr_tensor.predict(_input)
    return scaler_y.inverse_transform(ret).reshape(-1)


def predict_normalized(_input):
    scaler_y = load(scaler_path + "scaler_y")
    _input = np.array(_input)
    ret = svr_tensor.predict(_input)
    return scaler_y.inverse_transform(ret).reshape(-1)


def predict_normalized_no_inverse(_input):
    scaler_y = load(scaler_path + "scaler_y")
    _input = np.array(_input)
    ret = svr_tensor.predict(_input)
    return ret.reshape(-1)


def remove_trained_model():
    print(train_model_path)
    shutil.rmtree(train_model_path)


def train():
    return svr_tensor.main()
