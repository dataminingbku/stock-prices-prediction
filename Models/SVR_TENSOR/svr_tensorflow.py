import os
import tensorflow as tf
import numpy as np
from Preprocessing.preprocessing import pre_processing

export_path = os.path.dirname(os.path.abspath(
    __file__)) + "/../../trained-models/"


def svr_tensor(x_train, y_train, x_test, y_test, batch_size=50, epsilon=0.1, C=1, learning_rate=0.001):
    # Number of features
    n_features = x_train.shape[1]
    print("Features: ", n_features)

    # create graph
    sess = tf.Session()

    # Initialize placeholders
    x_data = tf.placeholder(
        shape=[None, n_features], dtype=tf.float32, name="x_data")
    y_target = tf.placeholder(
        shape=[None, 1], dtype=tf.float32, name="y_target")
    # Create variable
    W = tf.Variable(tf.random_normal(shape=[n_features, 1]))
    b = tf.Variable(tf.random_normal(shape=[1, 1]))

    # Declare model output
    model_output = tf.add(tf.matmul(x_data, W), b, name="y_output")
    # Declare loss function
    # L = 1/2*norm_2(W)^2 + C*sum(max(0, |y-y'| - epsilon))
    lam = tf.divide(1.0, 2.0 * C)
    norm_2_sq = tf.pow(tf.norm(W, ord=2), tf.constant(2.0))
    first_term = tf.multiply(lam, norm_2_sq)
    print(first_term.shape)
    second_term = tf.reduce_sum(tf.maximum(0., tf.subtract(
        tf.abs(tf.subtract(model_output, y_target)), epsilon)))
    print(second_term)
    loss = tf.add(first_term, second_term)
    print(loss)

    # Declare optimizer
    my_opt = tf.train.GradientDescentOptimizer(learning_rate)
    train_step = my_opt.minimize(loss)

    # Initialize variables
    init = tf.global_variables_initializer()
    sess.run(init)

    # Train
    train_loss = []
    test_loss = []
    tl = 1000
    while tl > 0.05:
        rand_index = np.random.choice(len(x_train), size=batch_size)
        X = x_train[rand_index]
        Y = np.transpose([y_train[rand_index]])
        sess.run(train_step, feed_dict={x_data: X, y_target: Y})
        temp_train_loss = sess.run(
            loss, feed_dict={x_data: x_train, y_target: np.transpose([y_train])})
        train_loss.append(temp_train_loss)
        temp_test_loss = sess.run(
            loss, feed_dict={x_data: x_test, y_target: np.transpose([y_test])})
        test_loss.append(temp_test_loss)
        tl = temp_test_loss
        print("Train loss: ", temp_train_loss, " Test loss: ", temp_test_loss)

    tf.saved_model.simple_save(session=sess, export_dir=export_path,
                               inputs={"x_data": x_data}, outputs={"y_output": model_output})
    return train_loss, test_loss


def predict(features):
    with tf.Session(graph=tf.Graph()) as sess:
        tf.saved_model.loader.load(sess=sess, tags=[tf.saved_model.tag_constants.SERVING],
                                   export_dir=export_path)
        graph = tf.get_default_graph()
        print(graph.get_operations())
        return sess.run('y_output:0', feed_dict={"x_data:0": features})


def main():
    x_train, y_train, x_test, y_test = pre_processing()
    return svr_tensor(x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test, C=100, epsilon=0.1,
                      learning_rate=0.001, batch_size=100)
