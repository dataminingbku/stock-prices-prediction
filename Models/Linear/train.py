import numpy as np
import time
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

def _train(M,N,data,offset,min_pred = 0):
    linear_reg = LinearRegression()
    y_pred = []
    for i in range(offset,M):
        x_train = data[i-N:i,[0,1,2,3,5]]
        y_train = data[i-N:i,4]
        linear_reg.fit(x_train,y_train)
        y = linear_reg.predict(data[i,[0,1,2,3,5]].reshape((1,-1)))
        y_pred.append(y[0])
    y_pred = np.array(y_pred)
    return y_pred

def train(M,data,num_train,N_max):
    RSME = []
    for N in range(1, N_max):
        y_pred = _train(M, N, data, num_train)
        error = mean_squared_error(data[num_train:, 4], y_pred)
        RSME.append(error)
    RSME = np.array(RSME)
    return np.argmin(RSME) + 1,RSME

