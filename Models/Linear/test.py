import numpy as np
from sklearn.metrics import mean_squared_error


def test(W,b,x_test,y_test):
    y_pred = (x_test.dot(W.T)+b).ravel().astype(np.float)+b
    y_test = y_test.ravel().astype(np.float)
    print(f"RMSE of model: ")
    print(mean_squared_error(y_test,y_pred))

