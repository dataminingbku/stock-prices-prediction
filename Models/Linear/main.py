"""
    This module descripes the options for running Stock Prices Prediction using
    SVM
"""
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
from Preprocessing.main import split_data,load_data
from .train import train,_train
from sklearn.metrics import mean_squared_error
from datetime import datetime
from .plot import plot,plot_trained_data,plot_loss,plot_predict
from sklearn.linear_model import LinearRegression
N_max = 30 #Linear Model using last N_max days to predict




def main(option):
    """ The main function """

    data = load_data()
    train_set,val_set,test_set = split_data(data)

    n_min = 0
    M = data.shape[0]
    num_train = train_set.shape[0]
    num_val = val_set.shape[0]
    num_test = test_set.shape[0]

    if option =='plot':
        plot(train_set[:,[0,1,2,3,5]],train_set[:,4],color = 'rx-',label='Train set')
        plot(val_set[:, [0, 1, 2, 3, 5]], val_set[:, 4], color='bx-', label='Validation set')
        plot(test_set[:, [0, 1, 2, 3, 5]], test_set[:, 4], color='gx-', label='Test set')
        plt.show()


    if option == 'train':


        n_min,RSME = train(M,data,num_train,N_max)
        print(f"The value of N make the RSMR minimum is {n_min}")

        fig,axes = plt.subplots(1,2)
        ax0 = axes[0]
        ax1 = axes[1]
        plot_loss(ax0,np.arange(1,N_max),RSME)
        plot_trained_data(ax1,num_train,num_val,n=np.argmin(RSME) + 1,data = data,label='validate')
        plt.show()
    elif option == 'test':
        n_min,RSME = train(M,data,num_train,N_max)
        y_pred = _train(M,n_min,data,num_train+num_val)
        y = data[num_train+num_val:,4].ravel()
        RSME = mean_squared_error(y_pred,y)
        print(f"RSME of test set: {RSME}")
        fig,axes = plt.subplots(1,1)
        ax = axes
        plot_trained_data(ax,num_train+num_val,num_test,n=n_min,data = data,label='test')
        plt.show()

    elif option == 'predict':
        print("Linear prediction ...")
        x = np.zeros((1,5))
        max_time = data[-1,0]
        day = datetime.strptime(
                str(input("Enter date DD/MM/YYYY: ")), "%d/%m/%Y")
        x[0,0] = day_pred = time.mktime(day.timetuple())
        x[0,1] = float(str(input("Enter open price: ")))
        x[0,2] = float(str(input("Enter high price: ")))
        x[0,3] = float(str(input("Enter low price: ")))
        x[0,4] = float(str(input("Enter volumn: ")))
        n_min,RSME = train(M,data,num_train,N_max)
        idx = np.where(data[:,0]<day_pred)[0][-n_min:]
        print(idx)
        train_data = data[idx,:]
        train_x = train_data[:,[0,1,2,3,5]]
        train_y = train_data[:,4]

        linear = LinearRegression()
        linear.fit(train_x,train_y)

        y_pred = linear.predict(x)
        print(f"Predicted Close Prize on {day}: {y_pred} VND")
        plot_predict(train_data,x,y_pred)







        # pred = x.dot(W.T) + b
        # print(f"Predicted price: {pred[0]}")
    else:
        print("Please enter one of the following arguments:")
        print("1. train")
        print("2. test")
        print("3. predict")

