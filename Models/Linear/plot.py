import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from pandas.plotting import register_matplotlib_converters
from sklearn.linear_model import LinearRegression
register_matplotlib_converters()


def plot(X,y,color,label):
    y = y.ravel()
    dt = X[:,0]
    dt_obj = list(map(datetime.fromtimestamp,dt))
    for x in dt_obj:
        x = str(x.date())

    plt.plot(dt_obj, y,color,label=label)
    plt.legend()
    plt.xlabel('Date')
    plt.ylabel('Close USD')


def plot_trained_data(plot,num_train,num_val,n,data,label):
    X_val = data[num_train:num_train+num_val,[0,1,2,3,5]]
    dt = X_val[:,0]
    dt_obj = list(map(datetime.fromtimestamp, dt))
    for x in dt_obj:
        x = str(x.date())
    y_val = data[num_train:num_train+num_val,4]
    y_pred = []
    linear = LinearRegression()
    for i in range(num_train,num_train+num_val):
        x_train = data[i-n:i,[0,1,2,3,5]]
        y_train = data[i-n:i,4]
        linear.fit(x_train,y_train)
        y = linear.predict(data[i,[0,1,2,3,5]].reshape(1,-1))
        y_pred.append(y[0])

    plot.plot(dt_obj,y_val,'rx-',label='Reality')
    plot.plot(dt_obj,y_pred,'bx',label='Prediction')
    plot.set_title(f'Prediction on {label} Data')
    plot.legend()

def plot_loss(plot,X,y):

    plot.plot(X, y, 'rx-', label='Loss')
    plot.set_xlabel('N')
    plot.set_ylabel('RMSE')
    plot.legend()
    plot.set_title('RSMR Loss according to N')

def plot_predict(train_data,x,y):

    x_train = train_data[:,[0,1,2,3,5]]
    y_train = train_data[:,4]
    N = x_train.shape[0]

    dt = x_train[:, 0]
    dt_obj = list(map(datetime.fromtimestamp, dt))
    d1 = str(datetime.fromtimestamp(x[0,0]).date())
    for d in dt_obj:
        d = str(d.date())
    plt.plot(dt_obj,y_train,'bx-',label=f'Price of last {N} days')
    plt.plot([dt_obj[0],d1],[y_train[0],y],'rx-',label='Price predicted')
    plt.legend()
    plt.show()

