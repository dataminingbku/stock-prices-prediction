"""
    This module defines PREPROCESSING stage
"""
import csv
import datetime
import time
import pandas as pd
import numpy as np
import sys

train_size = 0.6
validate_size = 0.2
test_size = 0.2


def load_data():
    link = sys.path[0] + '/Data/VCB.csv'
    df = pd.read_csv(link, header=1, names=["Date", "Sticker", "Open", "High", "Low", "Close", "Volumn"])
    df.loc[:, 'Date'] = pd.to_datetime(df['Date'], format='%m/%d/%Y')
    df.loc[:, 'Date'] = pd.to_timedelta(df['Date']).dt.total_seconds()
    df = df.values
    df = np.delete(df, 1, 1)
    return df


def split_data(df):

    N = df.shape[0]
    tr_N = int(N*train_size)
    val_N = int(N*validate_size)
    train_set = df[:tr_N,:]
    val_set = df[tr_N:tr_N+val_N,:]
    test_set = df[tr_N+val_N:,:]

    return train_set,val_set,test_set



def preprocessing(link):
    """ Function for preprocessing stage """
    _x = []
    _y = []
    file = open(link, "r")
    reader = csv.reader(file)
    next(reader)
    for row in reader:
        _x.append(convert_fields(row[:-1]))
        _y.append(float(row[-1]))

    file.close()
    return _x, _y


def convert_fields(_x):
    """ Function for converting fields into numbers """
    date = datetime.datetime.strptime(_x[0], '%m/%d/%Y')
    # Convert date to seconds
    _x[0] = time.mktime(date.timetuple())
    _x[2] = float(_x[2])
    _x[3] = float(_x[3])
    _x[4] = float(_x[4])
    _x[5] = float(_x[5])
    # Remove name
    del _x[1]
    return _x
