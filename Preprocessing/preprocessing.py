import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from joblib import dump

data_path = os.path.dirname(os.path.abspath(__file__)) + "/../Data/VCB.csv"

scaler_path = os.path.dirname(os.path.abspath(__file__)) + "/"


def load_data():
    data = pd.read_csv(data_path)
    return data


def feature_extraction(data):
    # Drop date variable
    data = data.drop(['<DATE>'], 1)
    # Drop symbol
    data = data.drop(['<TICKER>'], 1)
    # Move CLOSE column to end
    cols = list(data.columns)
    a, b = cols.index('<CLOSE>'), cols.index('<VOLUME>')
    cols[b], cols[a] = cols[a], cols[b]
    data = data[cols]
    # Dimensions of data
    n = data.shape[0]
    # Make data a numpy array
    data = data.values
    # Split data to train and test sets
    train_start = 0
    train_end = int(np.floor(0.8 * n))
    test_start = train_end
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    return data_train, data_test


def normalization_x(data_train, data_test):
    scaler_x = MinMaxScaler()
    data_train = scaler_x.fit_transform(data_train.astype(float))
    data_test = scaler_x.transform(data_test.astype(float))
    dump(scaler_x, scaler_path + "scaler_x")
    return data_train, data_test


def normalization_y(data_train, data_test):
    scaler_y = MinMaxScaler()
    data_train = scaler_y.fit_transform(data_train.astype(float))
    data_test = scaler_y.transform(data_test.astype(float))
    dump(scaler_y, scaler_path + "scaler_y")
    return data_train, data_test


def build_x_and_y(data_train, data_test):
    x_train = data_train[:, :-1]
    y_train = data_train[:, -1]
    x_test = data_test[:, :-1]
    y_test = data_test[:, -1]
    return x_train, y_train, x_test, y_test


def pre_processing():
    data = load_data()
    data_train, data_test = feature_extraction(data=data)
    x_train, y_train, x_test, y_test = build_x_and_y(data_train=data_train, data_test=data_test)
    x_train, x_test = normalization_x(data_train=x_train, data_test=x_test)
    y_train, y_test = normalization_y(data_train=np.array([[x] for x in y_train]),
                                      data_test=np.array([[x] for x in y_test]))
    y_train = y_train.reshape(-1)
    y_test = y_test.reshape(-1)
    return x_train, y_train, x_test, y_test


def get_date():
    data = load_data()
    return data['<DATE>'].values


if __name__ == '__main__':
    pre_processing()
