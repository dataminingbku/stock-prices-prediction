# Datamining Assignment - Stock Prices Prediction

### Preprocessing

This stage converts date into seconds and reads each number in the file as float, then returns X and Y with X is 2d array and Y is 1d array

This stage is required to run before training/testing

### Models

##### SVM

This model uses **rbf** kernel with **degree=5** (because there're 5 features: date, open, high, low, close)

### Usage

Modify ***main.py*** in **project directory** to run models (if not, models can't be run because of **import errors**)