"""
    This module define how to plot data
"""
import matplotlib.pyplot as plt
import datetime
import pandas as pd
import os
from joblib import load

scaler_path = os.path.dirname(
    os.path.abspath(__file__)) + "./Preprocessing/"


def convert_to_datetime(date):
    return pd.to_datetime(datetime.datetime.strptime(date, '%m/%d/%Y'), format='%m/%d/%Y')


def plot(date, y_train, y_test, y_train_pred, y_test_pred):
    """ Plotting data """
    pd.plotting.register_matplotlib_converters()
    plt.figure(figsize=(16, 9))
    x_train = []
    x_test = []
    scaler_y = load(scaler_path + "scaler_y")
    for i in range(len(y_train)):
        x_train.append(convert_to_datetime(date[i]))
    for i in range(len(y_train), len(y_train) + len(y_test)):
        x_test.append(convert_to_datetime(date[i]))
    y_train = scaler_y.inverse_transform([y_train]).reshape(-1)
    y_test = scaler_y.inverse_transform([y_test]).reshape(-1)
    plt.plot(x_train, y_train, 'b')
    plt.plot(x_train, y_train_pred, 'y')
    plt.plot(x_test, y_test, 'g')
    plt.plot(x_test, y_test_pred, 'r')
    plt.show()


def plot_loss(train_loss, test_loss):
    plt.figure(figsize=(16, 9))
    index = [x for x in range(len(train_loss))]
    plt.plot(index, train_loss, 'b')
    plt.plot(index, test_loss, 'r')
    plt.show()
